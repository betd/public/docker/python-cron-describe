from cron_descriptor import Options, CasingTypeEnum, DescriptionTypeEnum, ExpressionDescriptor
import sys

try:
	sys.argv[1]
except IndexError as error:
	sys.exit("Missing cron argument.")

try:
	locale = sys.argv[2]
except IndexError as error:
	locale = "en_GB"

options = Options()
options.throw_exception_on_parse_error = True
options.casing_type = CasingTypeEnum.Sentence
options.use_24hour_time_format = True
options.locale_code = locale
descripter = ExpressionDescriptor(sys.argv[1], options)

print(descripter.get_description(DescriptionTypeEnum.FULL))

