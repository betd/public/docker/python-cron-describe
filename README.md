# Docker image for cron description using https://pypi.org/project/cron-descriptor/

## Usage  

docker run --rm  registry.gitlab.com/betd/public/docker/python-cron-describe [expression] [locale]

with :  
* expression : cron expression ("31 0 1 * *" for example)
* locale : local for generated description. If undefined, default to "en_GB"
 

## Example
`docker run --rm registry.gitlab.com/betd/public/docker/python-cron-describe "31 0 1 * *" "fr_FR"`  
will output :  
`À 00:31, le 1 du mois`
