FROM python:3.7-alpine

WORKDIR /usr/local/bin

RUN pip install cron-descriptor

COPY cron-describe.py .

ENTRYPOINT ["python", "cron-describe.py"]

